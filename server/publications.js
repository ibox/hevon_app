/**
 * Created by ibox on 10/14/14.
 */
/*Meteor.publish('transaction_user', function(options) {
    return Transaction_user.find({}, options);
});*/

Meteor.publish('configuration', function(options) {
    return Configuration.find({}, options);
});

Meteor.publish('transaction_user', function() {
    return Transaction_user.find();
});

Meteor.publish("users", function () {
    return Meteor.users.find({});
});