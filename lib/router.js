/**
 * Created by ibox on 10/14/14.
 */
//layoutTemplate = default template for all routes
//loadingTemplate = template displaying while loading data
//the .subscribe runs just one time because after the data
// will be in miniMongo on client side
// --> the router takes care about the subscription
Router.configure({//TODO : add waitOn, beforeLoading etc...smoother
    layoutTemplate: 'layout',
    loadingTemplate: 'loading'/*,
    waitOn: function() {
        return [
            Meteor.subscribe('currentUser'),
            Meteor.subscribe('notifications')
            //comments, posts subscribed at "route" level,
            // not at "router" level
        ];}*/
});



//the route '/' is mapped to 'postsList'
//if we don't specify path it would map /postsList by default
//yield get the map by default (no connection needed)
Router.map(function() {
    this.route('home', {
        path: '/',
        action:function(){
            console.log('redirecting home...');
        }/*,
        controller: NewPostsListController*/
    });
    this.route('postSubmit', {
        path: '/submit',
        disableProgress: true
    });
    this.route('documentList', {
        path: '/documentList'
    });
    //-----------transacs
    this.route('transactions', {
        path: '/transactions'
    });
    this.route('itemEdit', {
        path: '/item/:_id/edit',
        data: function() {
            var temp = Transaction_user.findOne(this.params._id);
            /*for(var key in temp){
                console.log('Key : ' + key)
            }*/
            //var objArray
            return temp
        }
    });
    //------------users
    this.route('users', {
        path: '/users'
    });
    this.route('itemEdit2', {
        path: '/item2/:_id/edit',
        data: function() {
            var temp = Meteor.users.findOne(this.params._id);
            /*for(var key in temp){
             console.log('Key : ' + key)
             }*/
            //var objArray
            return temp
        }
    });


});