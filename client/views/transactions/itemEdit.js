/**
 * Created by ibox on 10/16/14.
 */
//TODO : collection name dynamic to have this template for any collection [LIST + ITEM + EDIT]
Template.itemEdit.events({
    'submit form': function(e) {
        e.preventDefault();
        var currentItemId = this._id;
        console.log('### Updating ID : ' + currentItemId)

        //var array = {};
        var pair = {};
        for(var key in this){
            if(key!="_id"){
                if(key=='conversion'){
                    var pair2 = {};
                    for(var key2 in this[key]){
                        pair2[''+key2] = $(e.target).find('[name=' + key2 + ']').val()
                        //pair['conversion.'+key2] = $(e.target).find('[name=' + key2 + ']').val()
                    }
                    pair['conversion'] = pair2; //TODO : implement $ when possible otherwise no sub doc update possible
                } else{
                    pair[''+key] = $(e.target).find('[name=' + key + ']').val()
                }
            }
        }
        //console.log('erUpdate2 : ' + JSON.stringify(pair));

        Transaction_user.update(currentItemId, {$set: pair},
            function(error) {
            if (error) {
                console.log('Error : ' + error.reason);
            } else {
                console.log('Update OK');
                Router.go('transactions');
            }
        });
    },
    'click .delete': function(e) {
        e.preventDefault();

        if (confirm("Delete this transaction?")) {
            var itemProperties = this._id;
            Transaction_user.remove(itemProperties);
            Router.go('transactions');
        }
    }
});

Template.itemEdit.helpers({
    keys:function(){
    var array = [];
    //var pair = {};
    for(var key in this){
        var keyStr = '' + key;
        var pair = {}; //reset every loop to not override 'index'
        //sub doc "conversion" TODO : can it be auto?
        if(keyStr=='conversion'){
            /*TODO : hidden until $ operator for sub doc update added to Meteor
            for(var key2 in this[keyStr]){
                var keyStr2 = '' + key2;
                var pair2 = {}; //reset every loop to not override 'index'
                pair2['index'] = 'conversion.'+keyStr2;
                pair2['value'] = this.conversion[keyStr2];
                array.push(pair2);
            }*/
        } else{
            pair['index'] = keyStr;
            pair['value'] = this[key];
            array.push(pair);
        }
    }
    return array;
}
});
