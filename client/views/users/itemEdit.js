/**
 * Created by ibox on 10/16/14.
 */
//TODO : collection name dynamic to have this template for any collection [LIST + ITEM + EDIT]
Template.itemEdit2.events({
    'submit form': function(e) {
        e.preventDefault();
        var currentItemId = this._id;
        console.log('### Updating ID : ' + currentItemId)

        //var array = {};
        var pair = {},pair2 = {},pair3 = {},pair4 = {};
        for(var key in this){
            if(key!="_id"){
                if(key=='services'){
                    //var pair2 = {}; //TODO : here or outside if
                    for(var key2 in this[key]){
                        pair2[''+key2] = $(e.target).find('[name=' + key2 + ']').val()
                        //pair['conversion.'+key2] = $(e.target).find('[name=' + key2 + ']').val()
                    }
                    pair['services'] = pair2; //TODO : implement $ when possible otherwise no sub doc update possible
                }
                else if(key=='emails') {
                    //var pair3 = {};
                    for(var key2 in this[key]){
                        pair3[''+key2] = $(e.target).find('[name=' + key2 + ']').val()
                    }
                    pair['emails'] = pair3;
                }else if(key=='profile') {
                    //var pair2 = {};
                    for(var key2 in this[key]){
                        if(key=='ccy'){
                            //TODO ############################# SAMEDI pour ccy
                        }else{
                            pair4[''+key2] = $(e.target).find('[name=' + key2 + ']').val()
                        }
                    }
                    pair['profile'] = pair4;
                }else{
                    pair[''+key] = $(e.target).find('[name=' + key + ']').val()
                }
            }
        }
        //console.log('erUpdate2 : ' + JSON.stringify(pair));

        Meteor.users.update(currentItemId, {$set: pair},
            function(error) {
            if (error) {
                console.log('Error : ' + error.reason);
            } else {
                console.log('Update OK');
                Router.go('users');
            }
        });
    },
    'click .delete': function(e) {
        e.preventDefault();

        if (confirm("Delete this user?")) {
            var itemProperties = this._id;
            Meteor.users.remove(itemProperties);
            Router.go('users');
        }
    }
});

Template.itemEdit2.helpers({
    keys:function(){
    var array = [];
    //var pair = {};
    for(var key in this){
        var keyStr = '' + key;
        var pair = {}; //reset every loop to not override 'index'
        //sub doc "conversion" TODO : can it be auto?
        if(keyStr=='services' || keyStr=='emails' || keyStr=='profile'){
            /*TODO : hidden until $ operator for sub doc update added to Meteor
            for(var key2 in this[keyStr]){
                var keyStr2 = '' + key2;
                var pair2 = {}; //reset every loop to not override 'index'
                pair2['index'] = 'conversion.'+keyStr2;
                pair2['value'] = this.conversion[keyStr2];
                array.push(pair2);
            }*/
        } else{
            pair['index'] = keyStr;
            pair['value'] = this[key];
            array.push(pair);
        }
    }
    return array;
}
});
