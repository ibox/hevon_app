Template.xeditable.rendered = function () {
    var container = this.$('*').eq(0);
    if (!container.hasClass('processed')) {
        container.addClass('processed');
        var options = _.extend(this.data, {
            // Default success function, saves document do database
            success: function (response, value) {
                var options = $(this).data().editable.options;

                //test -----------
                console.log('Options : ' + options.id +'/'+ options.value + '/'
                + value);

                //UPDATE
                if (options.collection && options.id && options.field) {
                    var update = {};
                    update[options.field] = value;
                    options.collection.update(options.id, {
                        $set: update
                        },
                        function(error,result) {
                            if (error) {
                                console.log('Erreur up : ' + error.reason);
                            }else {
                                console.log('Update OK : ' + result);
                            }
                        }
                    );
                }
            }
        });
        //make field username editable
        container.editable(options);
    }

    this.autorun(function () {
        //console.log('d----------------------fvdsfv');
        var value = Blaze.getData().value;
        var elData = container.data();
        if (elData && elData.editable) {
            console.log('VALUE : ' + value);
            elData.editable.setValue(value, true);
            // no idea why this is necessary; xeditable bug?
            if (elData.editableContainer)
                elData.editableContainer.formOptions.value = elData.editable.value;
        }
    });
}

Template.xeditable2.rendered = function () {

    var container = this.$('*').eq(0);
    if (!container.hasClass('processed')) {
        container.addClass('processed');
        var options = _.extend(this.data, {
            // Default success function, saves document do database
            success: function (response, value) {
                var options = $(this).data().editable.options;

                //test -----------
                console.log('Options : ' + options.id +'/'+ options.value + '/'
                    + value);

                //UPDATE
                if (options.collection && options.id && options.field) {
                    var update = {};
                    update[options.field] = value;
                    options.collection.update(options.id, {
                            $set: update
                        },
                        function(error,result) {
                            if (error) {
                                console.log('Erreur up : ' + error.reason);
                            }else {
                                console.log('Update OK : ' + result);
                            }
                        }
                    );
                }
            }
        });
        //make field username editable
        container.editable(options);
    }

    this.autorun(function () {
        //console.log('d----------------------fvdsfv');
        var value = Blaze.getData().value;
        var elData = container.data();
        if (elData && elData.editable) {
            console.log('VALUE : ' + value);
            elData.editable.setValue(value, true);
            // no idea why this is necessary; xeditable bug?
            if (elData.editableContainer)
                elData.editableContainer.formOptions.value = elData.editable.value;
        }
    });
}