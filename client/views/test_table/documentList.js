/*Template.document.titleOptions = function () {
    return {
        // We need to specify collection, id and field to autoupdate MongoDb
        collection: Transaction_user,
        id: this._id,
        field: 'username',
        value: this.username
    }
}*/

Template.documentList.helpers({
    documents: function() {
        var transacsList = Transaction_user.find();
        /*transacsList.forEach(function (doc) {
            console.log("test table2 ID : " + doc._id);
            console.log("ccy_from >> : " + doc.ccy_from);
        });*/
        return transacsList;
    }
});

Template.document.helpers({
    titleOptions: function() {
        return {
            // We need to specify collection, id and field to autoupdate MongoDb
            collection: Transaction_user,
            id: this._id,
            field: 'ccy_from',
            value: this.ccy_from
        }
    }
});

// #### NOTES
/*
 email --> user.emails[0].address
 */