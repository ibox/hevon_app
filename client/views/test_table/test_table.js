dataTableData = function () {
    return Meteor.users.find().fetch(); // or .map()
};

var optionsObject = {
    columns: [{
        title: 'ID',
        data: '_id'//, // note: access nested data like this
        //className: 'nameColumn'
    },{
        title: 'Name',
        data: 'profile.name'
        //render: renderPhoto, // optional data transform, see below
       // className: 'imageColumn'
    },{
        title: 'IBAN',
        data: 'profile.IBAN'
    }]
    // ... see jquery.dataTables docs for more
}

Template.test.helpers({
    reactiveDataFunction: function () {
        return dataTableData;
    },
    optionsObject: optionsObject // see below
});